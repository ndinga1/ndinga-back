from django.contrib import admin

from .models import Langue

# Register your models here.

@admin.register(Langue)
class LangueAdmin(admin.ModelAdmin):
    list_display = ['fr', 'ki', 'token']
