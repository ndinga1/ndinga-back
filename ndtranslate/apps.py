from django.apps import AppConfig


class NdtranslateConfig(AppConfig):
    name = 'ndtranslate'
