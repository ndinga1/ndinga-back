from csv import DictReader

from django.core.management import BaseCommand
from ndtranslate.models import Langue, Mots

class Command(BaseCommand) :
    help = "Chargement des données depuis le fichier csv"

    def handle(self, *args,**options):
        if Langue.objects.exists():
            print("Données de traduction déjà présente")
            return 
        print("Chargement des données")
        for row in DictReader(open('./ki-fr.csv')):
            langue = Langue()
            langue.fr = row["fr"]
            langue.ki = row["ki"]
            langue.token = row["token"]
            langue.save()

