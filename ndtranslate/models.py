from django.db import models

# Create your models here.

class Langue(models.Model):
    ki = models.CharField(max_length=100)
    fr = models.CharField(max_length=100)
    token = models.CharField(max_length=22,blank=False)

class Mots(models.Model):
    mot = models.CharField(max_length=100)
    token = models.CharField(max_length=22,blank=False)